<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\LearnPath;
use App\Models\Library;
use Illuminate\Http\Request;

class LearnPathController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth', ['except' => ['index', 'show', 'show_category']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Factory|Response|View
     */
    public function index(Request $request)
    {
        $libraries = Library::all();

        return view('learn_paths.index', [
            'libraries' => $libraries,
            'selected_library' => 'all',
        ]);
    }

    public function show_category(Request $request, $library_slug)
    {
        $libraries = Library::all();

        return view('learn_paths.index', [
            'libraries' => $libraries,
            'selected_library' => $library_slug,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param $library_slug
     * @param $learn_path_slug
     * @return Factory|RedirectResponse|Response|View
     */
    public function show($library_slug, $learn_path_slug)
    {
        $path = LearnPath::firstWhere('slug', '=', $learn_path_slug);
        if ($path) {

            // $view = new \App\View(['type' => 2, 'item_id' => $path->id]);
            // $view->save();

            $authors = array();
            foreach ($path->courses as $course) {
                foreach ($course->authors as $author) {
                    array_push($authors, $author->id);
                }
            }
            $authors = Author::find($authors);
            $authors = array_values($authors->all());
            $path_state = (new CartController())->isAdded('2-' . $path->id);
            return view('learn_paths.show', [
                'path' => $path,
                'courses' => $path->courses,
                'authors' => $authors,
                'path_state' => $path_state,
            ]);
        }
        abort(404);
        return redirect()->route('root.home');
    }
}
