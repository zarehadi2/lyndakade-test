<?php

namespace App\Http\Controllers;

use App\Models\CourseView;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DataController extends Controller
{

    public function views_api(Request $request)
    {
        // $views = View::groupBy('ip')->groupBy('user_id')->get();
        $views = CourseView::distinct('ip')->get(['ip', 'created_at', 'user_id']);
        if (!$views) {
            return new JsonResponse([
                'data' => []
            ], 404);
        }
        return new JsonResponse([
            'data' => $views->toArray(),
        ], 200);
    }
}
