<?php

namespace App\Http\Controllers;

use App\Models\Paid;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaidController extends Controller
{

    /**
     * return array of views for passed days
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function get_chart_price_data(Request $request)
    {
        return $this->get_chart_data($request, 'price');
    }

    /**
     * return array of views for passed days
     *
     * @param Request $request
     * @param $type
     * @return JsonResponse
     */
    private function get_chart_data(Request $request, $type)
    {
        $days = $request->get('days', 7);
        $response = array();

        $response['all_time_count'] = Paid::all()->count();

        $response['all_time_price'] = 0;
        foreach (Paid::all() as $item)
            $response['all_time_price'] += $item->price;

        $i = $days;
        $listed_count = 0;
        $listed_price = 0;
        while ($i--) {
            $eventsForThisDay = DB::select('select * from paids' .
                ' where paids.created_at<"' . Carbon::tomorrow()->subDays($i)->toDateString() . '"' .
                ' and paids.created_at>="' . Carbon::today()->subDays($i)->toDateString() . '"');

            $data = array();
            $data['count'] = count($eventsForThisDay);
            $data['total'] = 0;
            foreach ($eventsForThisDay as $item)
                $data['total'] += $item->price;
            $listed_count += $data['count'];
            $listed_price += $data['total'];

            $response['data_count'][$days - $i] = $data['count'];
            $response['data_price'][$days - $i] = $data['total'];
            $response['label'][$days - $i] = Carbon::today()->subDays($i)->toDateString();
        }

        $response['total_count'] = $listed_count;
        $response['total_price'] = $listed_price;

        return response()->json($response);
    }

    /**
     * return array of views for passed days
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function get_chart_count_data(Request $request)
    {
        return $this->get_chart_data($request, 'count');
    }

    public function isPaid($item_id, $user_id, $type)
    {
        return Paid::all()
            ->where('user_id', $user_id)
            ->where('item_id', $item_id)
            ->where('type', $type);
    }
}
