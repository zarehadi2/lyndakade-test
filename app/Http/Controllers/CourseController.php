<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\CourseView;
use App\Models\LearnPath;
use App\Models\SkillLevel;
use Faker\Provider\Uuid;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CourseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     * @throws Throwable
     */
    public function index()
    {
        return view('courses.index-logged-out');
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $subject
     * @param $slug
     * @param $my_id
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function show(Request $request, $subject, $slug, $my_id)
    {
        $course = Course::find($my_id);
        if ($course) {
            if ($course->slug == $slug) {
                $course->increment('views');
                // views($course)->record();

                $view = new CourseView([
                    'course_id' => $course->id,
                    'user_id' => auth()->check() ? auth()->id() : null,
                    'ip' => $request->ip(),
                ]);
                $view->save();

                /*
                 * getting courses id related to subjects
                 */
                $subjects = $course->subjects;
                $subjects_id = array();
                foreach ($subjects as $subject) {
                    array_push($subjects_id, $subject->id);
                }
                $courses_id = DB::table('course_subject')
                ->whereIn('subject_id', $subjects_id)
                    ->get('course_id');

                $ids = array();
                foreach ($courses_id as $id) {
                    array_push($ids, $id->course_id);
                }

                /*
                 * getting courses id related to software
                 */
                $softwares = $course->softwares;
                $softwares_id = array();
                foreach ($softwares as $software) {
                    array_push($softwares_id, $software->course_id);
                }
                $courses_id = DB::table('course_software')
                ->whereIn('software_id', $softwares_id)
                    ->get('course_id');

                foreach ($courses_id as $id) {
                    array_push($ids, $id->course_id);
                }

                while (($key = array_search($my_id, $ids)) !== false) {
                    unset($ids[$key]);
                }
                $ids = array_values(array_unique($ids));

                $related_courses = Course::orderBy('views', 'DESC')->whereIn('id', $ids)->limit(50)->get();

                $courses = array();
                foreach ($related_courses as $related_course) {
                    array_push($courses, $related_course);
                }

                $skillEng = SkillLevel::find($course->skillLevel)->titleEng;
                $skill = SkillLevel::find($course->skillLevel)->title;
                return view('courses.show', [
                    'skill' => $skill,
                    'skillEng' => $skillEng,
                    'course' => $course,
                    'related_courses' => $courses,
                    'course_state' => get_course_state($course), // 1 = purchased,  2 = added to cart, 3 = not added to cart
                ]);
            }
        }
        abort(404);
        return redirect()->route('root.home');
    }

    public function not_found()
    {
        return abort(404);
    }


    public function mycourses()
    {
        $courses_id = [];

        foreach (Auth::user()->paids as $paid) {
            if ($paid->type == '1') {
                array_push($courses_id, $paid->item_id);
            } else {
                $learn_path = LearnPath::find($paid->item_id);
                foreach ($learn_path->courses as $course) {
                    array_push($courses_id, $course->id);
                }
            }
        }
        $courses_id = array_unique($courses_id);

        $courses = Course::find($courses_id);

        return view('courses.my-courses', [
            'courses' => $courses,
        ]);
    }

    public function subtitle_content(Request $request)
    {
        $course_id = $request->get('courseId');
        $video_id = $request->get('videoId');
        $course = Course::find($course_id);
        $subtitle = json_decode($course->previewSubtitle);

        foreach ($subtitle as $file) {
            $content = Storage::disk('FTP')->get($file->download_link);
            return '<pre>' . $content . '</pre>';
        }
        return '';
    }

    public function course_api($id)
    {
        $course = Course::find($id);
        if (!$course) {
            return new JsonResponse([
                'data' => []
            ], 404);
        }
        return new JsonResponse([
            'data' => $course->toArray(),
            'url' => courseURL($course),
        ], 200);
    }

    public function courses_api()
    {
        $courses = Course::orderBy('created_at', 'desc')->limit(20)->get();
        if (!$courses) {
            return new JsonResponse([
                'data' => []
            ], 404);
        }
        return new JsonResponse([
            'data' => $courses->toArray(),
        ], 200);
    }

    private function find_id_from_link($link)
    {
        if ($idx = strpos($link, 'lynda.com')) {
            $link = substr($link, $idx);
        }
        if ($idx = strpos($link, 'lyndakade.ir')) {
            $link = substr($link, $idx);
        }
        try {
            $id = explode("/", $link)[3];
            if ($idx = strpos($id, '-')) {
                $id = substr($id, 0, $idx);
            }
            return $id;
        } catch (\Throwable $th) {
            return false;
        }
        return false;
    }

    public function courses_with_link_api(Request $request)
    {
        $link = $request->get('link');
        if (!$link) {
            return new JsonResponse([
                'data' => [],
                'status' => 'link is required',
            ], 200);
        }
        $id = $this->find_id_from_link($link);
        if (!$id) {
            return new JsonResponse([
                'data' => [],
                'status' => 'link is not valid',
            ], 200);
        }
        $course = Course::find($id);
        if (!$course) {
            return new JsonResponse([
                'data' => [],
                'status' => 'course was not found',
            ], 200);
        }
        return new JsonResponse([
            'data' => $course->toArray(),
            'url' => courseURL($course),
            'status' => 'success',
        ], 200);
    }

    public function download_course(Request $request, $id)
    {
        $course = Course::find($id);

        function fix_path_link($file_path)
        {
            $file_path = str_replace('%20', ' ', $file_path);
            $file_path = str_replace('%28', '(', $file_path);
            $file_path = str_replace('%29', ')', $file_path);
            $file_path = str_replace('%2C', ',', $file_path);
            if ($file_path[strlen($file_path) - 1] == ' ') {
                $file_path = substr($file_path, 0, strlen($file_path) - 1);
            }
            return $file_path;
        }

        function get_file_from_ftp($course, $file_type)
        {
            $file_path = json_decode($course->{$file_type})[0]->download_link;
            $file_path = fix_path_link($file_path);
            $file_name = json_decode($course->{$file_type})[0]->original_name;
            $file_path_clone = explode('/', '/' . $file_path);
            $file_path_clone = array_splice($file_path_clone, 0, count($file_path_clone) - 1);
            $file_path_clone = join('/', $file_path_clone);
            if ($file_type == 'previewFile') {
                $file_name = 'previewFile.mp4';
            }
            if ($file_type == 'previewSubtitle') {
                $file_name = 'previewSubtitle.srt';
            }

            return redirect(fromDLHost($file_path) . '?' . hash('sha512', Uuid::uuid()));

            // $ftp = Storage::createFtpDriver([
            //     'host'     => 'dl.lyndakade.ir',
            //     'username' => 'pz11914',
            //     'password' => 'ashkan1996',
            //     'port'     => '21',
            //     'timeout'  => 9999999,
            //     'root' => 'public_html',
            //     'disable_asserts' => true,
            // ]);
            // return $ftp->download('./' . $file_path);

            // return $ftp->response($file_path, $file_name, [], 'attachment');


            // return Storage::disk('FTP')->download('/' . $file_path, $file_name, [
            //     'Accept-Ranges' => 'bytes',
            //     // 'Content-Disposition' => 'inline',
            //     // 'Content-Type' => 'application/octet-stream',
            // ]);


            // return response()->stream(function () use ($file_path) {
            //     echo file_get_contents(fromDLHost($file_path));
            // }, 200, ['Accept-Ranges' => 'bytes']);


            // return response()->download(fromDLHost($file_path), $file_name, [
            //     'Accept-Ranges' => 'bytes',
            //     'Content-Disposition' => 'inline',
            //     'Content-Type' => 'application/octet-stream',
            // ]);


            // $fs = Storage::disk('FTP');
            // // dd($fs->allFiles($file_path_clone));
            // $stream = $fs->readStream('/' . $file_path);
            // $mime = explode('.', $file_name);
            // $mime = $mime[count($mime) - 1];
            // return response()->stream(
            //     function () use ($stream) {
            //         fpassthru($stream);
            //     },
            //     200,
            //     [
            //         'Accept-Ranges' => 'bytes',
            //         'Content-Type' => $mime,
            //         'Content-disposition' => 'attachment; filename="' . $file_name . '"',
            //     ]
            // );
        }
        $previewFile = $request->input(hash('md5', 'previewFile'));
        if ($previewFile && hash('sha256', auth()->id()) == $previewFile) {
            return get_file_from_ftp($course, 'previewFile');
        }
        $previewSubtitle = $request->input(hash('md5', 'previewSubtitle'));
        if ($previewSubtitle && hash('sha256', auth()->id()) == $previewSubtitle) {
            return get_file_from_ftp($course, 'previewSubtitle');
        }
        $courseFile = $request->input(hash('md5', 'courseFile'));
        if ($courseFile && hash('sha256', auth()->id()) == $courseFile) {
            return get_file_from_ftp($course, 'courseFile');
        }
        $exFiles = $request->input(hash('md5', 'exFiles'));
        if ($exFiles && hash('sha256', auth()->id()) == $exFiles) {
            $filename = $request->get('filename');
            $filename = fix_path_link($filename);
            foreach (json_decode($course->exerciseFile) as $file) {
                if ($filename == fix_path_link($file->original_name)) {
                    $file_path = $file->download_link;
                    $file_path = fix_path_link($file_path);

                    return redirect(fromDLHost($file_path) . '?' . hash('sha512', Uuid::uuid()));
                    // return Storage::disk('FTP')->download('/public_html/' . $file->download_link, $file->original_name, [
                    //     'Content-Disposition' => 'inline'
                    // ]);
                }
            }
        }
        return new JsonResponse([], 404);
    }

    public function newest(Request $request)
    {
        $courses = Course::orderBy('created_at', 'DESC')->get();

        if ($request->ajax()) {
            $page = $request->get('page', null);
            if (!$page) {
                return [];
            }
            $res = [];
            foreach ($courses->forPage(intval($page), 40) as $course) {
                $res[] = $this->get_course_tile($course->id);
            }
            return $res;
        }
        return view('courses.date', ['courses' => $courses->forPage(1, 40), 'coursetype' => 'newest']);
    }

    public function best(Request $request)
    {
        $courses = Course::orderBy('views', 'desc')->get();

        if ($request->ajax()) {
            $page = $request->get('page', null);
            if (!$page) {
                return [];
            }
            $res = [];
            foreach ($courses->forPage(intval($page), 40) as $course) {
                $res[] = $this->get_course_tile($course->id);
            }
            return $res;
        }
        return view('courses.date', ['courses' => $courses->forPage(1, 40), 'coursetype' => 'best']);
    }

    public function free(Request $request)
    {
        $courses = Course::where('price', 0)->get();
        if ($request->ajax()) {
            $page = $request->get('page', null);
            if (!$page) {
                return [];
            }
            $res = [];
            foreach ($courses->forPage(intval($page), 40) as $course) {
                $res[] = $this->get_course_tile($course->id);
            }
            return $res;
        }
        return view('courses.date', ['courses' => $courses->forPage(1, 40), 'coursetype' => 'free']);
    }

    public function get_course_tile($id)
    {
        if (Course::find($id))
            return view('courses.partials._course_list_grid', ['course' => Course::find($id)])->render();
        return 'not found';
    }

    public function add_view(Request $request, $id)
    {
        $course = Course::where('id', $id)->get()->first();
        if ($course) {
            $course->increment('views');

            return new JsonResponse([
                'status' => 'success',
                'message' => 'view is added successfully to ' . $course->titleEng,
            ], 200);
        }
        return new JsonResponse([
            'status' => 'failed',
            'message' => 'course was not found',
        ], 200);
    }

    public function add_views(Request $request, $id, $count)
    {
        $course = Course::where('id', $id)->get()->first();
        if ($course) {
            $count = intval($count);
            while ($count > 0) {
                $course->increment('views');
                $count--;
            }

            return new JsonResponse([
                'status' => 'success',
                'message' => 'view is added successfully to ' . $course->titleEng,
            ], 200);
        }
        return new JsonResponse([
            'status' => 'failed',
            'message' => 'course was not found',
        ], 200);
    }

    public function courses_all_api(Request $request)
    {
        $courses =  Course::orderBy('created_at', 'desc')->get();

        $page = intval($request->get('page', 1));
        $perPage = intval($request->get('perPage', 15));
        if ($perPage > 50)
            $perPage = 50;
        $n = count($courses) / $perPage;
        if ($n - (int)$n > 0)
            $n = (int)$n + 1;
        if ($page > $n)
            $page = $n;
        $courses = $courses->forPage($page, $perPage);
        return new JsonResponse([
            'data' => $courses->toArray(),
            'page' => $page,
            'perPage' => $perPage,
            'total_pages' => $n,
        ], 200);
    }
}
