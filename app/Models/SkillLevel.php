<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SkillLevel extends Model
{
    use HasFactory;

    public function courses()
    {
        return Course::where('skillLevel', $this->id)->get();
        // return $this->hasMany(Course::class);
    }
}
