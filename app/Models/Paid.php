<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paid extends Model
{
    use HasFactory;
    protected $fillable = ['factorId', 'type', 'item_id', 'user_id', 'price'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
