<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// courses
Route::get('/', 'App\Http\Controllers\CourseController@index')->name('root.home');
Route::get('/{slug_url}/{slug}/{id}-2.html', 'App\Http\Controllers\CourseController@show')->name('courses.show');
Route::get('/ajax/player/transcript', 'App\Http\Controllers\CourseController@subtitle_content')->name('courses.subtitle_content');
Route::get('courses/download/{id}', 'App\Http\Controllers\CourseController@download_course')->name('courses.download');

// authors
Route::get('/authors', 'App\Http\Controllers\AuthorController@index')->name('authors.index');
Route::get('{name}/{id}-1.html', 'App\Http\Controllers\AuthorController@show')->name('authors.show');


// subjects & software & libraries
Route::get('/{slug}/{id}-0.html', 'App\Http\Controllers\HomeController@show')->name('home.show');


// needs to be logged in, for request course
Route::group(['middleware' => 'auth'], function () {
    Route::name('demands.')->group(function () {
        Route::get('/request-course', 'App\Http\Controllers\DemandController@create')->name('create');
        Route::post('/request-course', 'App\Http\Controllers\DemandController@store')->name('store');
    });
});

// learning paths
Route::get('learning-path/', 'App\Http\Controllers\LearnPathController@index')->name('learn.paths.index');
Route::get('learning-path/{library_slug}/{learn_path_slug}', 'App\Http\Controllers\LearnPathController@show')->name('learn.paths.show');
// "see all" button, for each library, in navbar
Route::get('learning-path/{library_slug}', 'App\Http\Controllers\LearnPathController@show_category')->name('learn.paths.show_category');

// logging with google account
Route::get('auth/google', 'App\Http\Controllers\Auth\LoginController@redirectToGoogle')->name('login.google');
Route::get('auth/google/callback', 'App\Http\Controllers\Auth\LoginController@handleGoogleCallback')->name('login.google.callback');

Route::get('/contact-us', 'App\Http\Controllers\HomeController@contact_us')->name('root.contact.us');
Route::post('/contact-us', 'App\Http\Controllers\HomeController@contact_us_post')->name('root.contact.us.post');
Route::get('/search', 'App\Http\Controllers\HomeController@search_page')->name('search');

Route::group(['middleware' => 'auth'], function () {
    Route::name('cart.')->group(function () {
        Route::get('/cart/', 'App\Http\Controllers\CartController@index')->name('index');
        Route::post('/cart/', 'App\Http\Controllers\CartController@index')->name('index');
        Route::post('/cart/add', 'App\Http\Controllers\CartController@add')->name('add');
        Route::post('/cart/remove', 'App\Http\Controllers\CartController@remove')->name('remove');
        Route::post('/cart/remove/all', 'App\Http\Controllers\CartController@remove_all')->name('remove_all');
    });

    Route::get('payment/send', 'App\Http\Controllers\CartController@send_to_pay')->name('payment.send');
    Route::post('payment/redirect', 'App\Http\Controllers\CartController@redirect')->name('payment.redirect');
    Route::get('payment/callback', 'App\Http\Controllers\CartController@pay_callback')->name('payment.callback');

    Route::get('my-courses', 'App\Http\Controllers\ProfileController@my_courses')->name('courses.mycourses');
});


//Route::fallback(function () {
//    $course = new CourseController();
//    return $course->not_found();
//});
Route::fallback('App\Http\Controllers\CourseController@not_found');

// get data for charts in admin dashboard
Route::post('data/get-views-for-days', 'App\Http\Controllers\ChartDataController@get_chart_data')->name('views.data');
Route::post('data/get-purchases-count-for-days', 'App\Http\Controllers\ChartDataController@get_chart_count_data')->name('purchases.count.data');
Route::post('data/get-purchases-price-for-days', 'App\Http\Controllers\ChartDataController@get_chart_price_data')->name('purchases.price.data');

Route::get('profile', 'App\Http\Controllers\UserController@my_profile')->name('my-profile');
Route::get('profile/edit', 'App\Http\Controllers\UserController@edit')->name('my-profile.edit');
Route::post('profile/edit', 'App\Http\Controllers\UserController@update')->name('my-profile.update');

Route::post('users/username/check', 'App\Http\Controllers\UserController@username_check')->name('users.username-check');

Route::get('courses/newest', 'App\Http\Controllers\CourseController@newest')->name('courses.newest');
Route::get('courses/best', 'App\Http\Controllers\CourseController@best')->name('courses.best');
Route::get('courses/free', 'App\Http\Controllers\CourseController@free')->name('courses.free');

Route::get('/454247.txt', function () {
    $fileText = "";
    $myName = "454247.txt";
    $headers = [
        'Content-type' => 'text/plain',
        'Content-Disposition' => sprintf('attachment; filename="%s"', $myName),
        'Content-Length' => strlen($fileText)
    ];
    return response()->make($fileText, 200, $headers);
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
